<?php

namespace App\DataFixtures;

use App\Entity\Users;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixture
{

    /**
     * @var \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    protected function loadData(ObjectManager $manager): void
    {
        $this->createMany(10, 'main_users', function ($i) {
            $user = new Users();
            $user->setName($this->faker->firstName);
            $user->setLastName($this->faker->lastName);
            $user->setEmail(sprintf('user%d@comersantander.com', $i));
            $user->setPhone($this->faker->phoneNumber);
            $user->setCode($this->faker->unixTime());
            $user->setCompany($this->faker->company);
            $user->setCity($this->faker->city);
            $user->setBirthdate($this->faker->dateTime('-18 years', 'America/Bogota'));
            $user->setPassword($this->encoder->encodePassword($user, 's0p0rt3.'));

            return $user;
        });

        $manager->flush();
    }
}
